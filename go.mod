module steampipe-plugin-hibp

go 1.16

require (
	github.com/turbot/steampipe-plugin-sdk v0.2.10
	gitlab.com/wedtm/go-hibp v0.0.0-20210624142320-1d7159aa0291
)
